package testlogin;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static testlogin.UserStrings.LOGIN_PAGE_X;
import static testlogin.UserStrings.HOME_PAGE_X;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class AuthFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) { }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession ses = req.getSession(false);

        String reqURI = req.getRequestURI();
        if(SessionUtils.getLoggedIn(ses) && reqURI.contains("/" + LOGIN_PAGE_X)) {
            try {
                resp.sendRedirect(req.getContextPath() + "/" + HOME_PAGE_X);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (reqURI.contains("/" + LOGIN_PAGE_X)
                || reqURI.contains("/public/")
                || reqURI.contains("javax.faces.resource")
                || (SessionUtils.getUserName(ses) != null)) {
            try {
                chain.doFilter(request, response);
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
        } else {
            try {
                resp.sendRedirect(req.getContextPath() + "/" + LOGIN_PAGE_X);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void destroy() { }
}
