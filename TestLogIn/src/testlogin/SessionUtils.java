package testlogin;

import org.jetbrains.annotations.Nullable;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static testlogin.UserStrings.LOGGED_IN_ATTRIBUTE;
import static testlogin.UserStrings.USERNAME_ATTRIBUTE;

public class SessionUtils {
    public static HttpSession getCurrentSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    public static HttpServletRequest getCurrentRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    @Nullable
    public static String getCurrentUserName() {
        return getUserName(getCurrentSession());
    }

    @Nullable
    public static String getUserName(HttpSession session) {
        if (session != null){
            Object username = session.getAttribute(USERNAME_ATTRIBUTE);
            if(username instanceof String) {
                return username.toString();
            }
        }
        return null;
    }

    public static boolean getCurrentLoggedIn() {
        return getLoggedIn(getCurrentSession());
    }

    public static boolean getLoggedIn(HttpSession session) {
        if (session != null){
            Object loggedIn = session.getAttribute(LOGGED_IN_ATTRIBUTE);
            if(loggedIn instanceof Boolean) {
                return (boolean) loggedIn;
            }
        }
        return false;
    }
}
