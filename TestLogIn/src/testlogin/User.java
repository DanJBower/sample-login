package testlogin;

import javax.enterprise.context.SessionScoped;
import javax.faces.annotation.FacesConfig;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static testlogin.UserStrings.TABLE_NAME;
import static testlogin.UserStrings.USER_NAME_COL;
import static testlogin.UserStrings.USER_PASS_COL;
import static testlogin.UserStrings.LOGIN_PAGE;
import static testlogin.UserStrings.LOGOUT_PAGE;
import static testlogin.UserStrings.USERNAME_ATTRIBUTE;
import static testlogin.UserStrings.LOGGED_IN_ATTRIBUTE;

@FacesConfig
@Named(value = "user")
@SessionScoped
public class User implements Serializable {
    private String name, pass;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    //Returns the landing page for the action
    public String tryLoggingIn() {
        if (validateUserDetails()) {
            HttpSession session = SessionUtils.getCurrentSession();
            session.setAttribute(USERNAME_ATTRIBUTE, getName());
            session.setAttribute(LOGGED_IN_ATTRIBUTE, true);
            return LOGOUT_PAGE;
        } else {
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_WARN,
                            "Incorrect Username and Password",
                            "Please enter correct username and Password"));
            return LOGIN_PAGE;
        }
    }

    //Check user details against database
    private boolean validateUserDetails() {
        Connection con = DataConnect.getConnection();

        try {
            //If con is null, can't continue
            if(con == null) {
                System.out.println("No SQL Database Connection!");
                return false;
            }

            PreparedStatement ps = con.prepareStatement("Select " + USER_NAME_COL + ", " + USER_PASS_COL + " from "
                    + TABLE_NAME + " where " + USER_NAME_COL + " = ? and "+ USER_PASS_COL + " = ?;");
            ps.setString(1, getName()); //Replaces first ?
            ps.setString(2, getPass()); //Replaces second ?

            //Use SQL to get all case insensitive matches
            ResultSet rs = ps.executeQuery();

            //Go through all matches checking case. Making both
            //username and password case sensitive.
            while (rs.next()) {
                if(rs.getString(USER_NAME_COL).equals(getName())) {
                    if(rs.getString(USER_PASS_COL).equals(getPass())) {
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            System.out.println("Login error -->" + ex.getMessage());
        } finally { //Still executes after return. See https://stackoverflow.com/q/65035/4601149
            if(con != null) {
                DataConnect.close(con);
            }
        }
        return false;
    }

    public String logout() {
        name = "";
        HttpSession session = SessionUtils.getCurrentSession();
        session.setAttribute(USERNAME_ATTRIBUTE, getName());
        session.setAttribute(LOGGED_IN_ATTRIBUTE, false);
        session.invalidate();
        return LOGIN_PAGE;
    }
}
