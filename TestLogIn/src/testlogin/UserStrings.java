package testlogin;

//Constants for a lot strings used for login
public final class UserStrings {
    //Make it so class can't be initialised
    private UserStrings() { }

    public static final String USER_NAME_COL = "USER_NAME";
    public static final String USER_PASS_COL = "USER_PASS";
    public static final String TABLE_NAME = "jono.users";

    public static final String DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DATABASE_ADDRESS = "jdbc:mysql://localhost:3306/jono?autoReconnect=true&useSSL=false";
    public static final String DATABASE_USER = "root";
    public static final String DATABASE_PASS = "1974568360";

    public static final String LOGOUT_PAGE = "logout";
    public static final String LOGIN_PAGE = "login";
    public static final String HOME_PAGE_X = "index.xhtml";
    public static final String LOGIN_PAGE_X = LOGIN_PAGE + ".xhtml";

    public static final String USERNAME_ATTRIBUTE = "username";
    public static final String LOGGED_IN_ATTRIBUTE = "logged_in";
}
