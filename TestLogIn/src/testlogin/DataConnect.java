package testlogin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static testlogin.UserStrings.DATABASE_ADDRESS;
import static testlogin.UserStrings.DATABASE_DRIVER;
import static testlogin.UserStrings.DATABASE_USER;
import static testlogin.UserStrings.DATABASE_PASS;


public class DataConnect {
    public static Connection getConnection() {
        try {
            Class.forName(DATABASE_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Connection con = null;
        try {
            con = DriverManager.getConnection(DATABASE_ADDRESS, DATABASE_USER, DATABASE_PASS);
        } catch (SQLException e) {
            System.out.println("Connection error -->" + e.getMessage());
        }
        return con;
    }

    public static void close(Connection con) {
        try {
            con.close();
        } catch (SQLException e) {
            System.out.println("Connection close error -->" + e.getMessage());
        }
    }
}
